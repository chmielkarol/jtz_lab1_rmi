///////////////
//Karol Chmiel
//Nr albumu: 235035
//Termin: pon 11:15 - 13:00
///////////////

package lab01;

import java.awt.EventQueue;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.Date;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.Serializable;
import java.awt.event.ActionEvent;

public class Bramka implements IBramka, Serializable{

	private static final long serialVersionUID = -6655302289674723279L;
	
	private JFrame frmBramka;
	private JButton btnStop, btnStart, btnWe, btnWy;
	
	private int ID;
	
	private Registry reg;
	private ICentrala icentrala;
	private IBramka ibramka;
	
	private int we, wy; 

	@Override
	public int[] getStatystyka(Date pocz, Date kon) throws RemoteException {
		int[] statystyka = {we, wy};
		return statystyka;
	}

	@Override
	public boolean zamknijBramke() throws RemoteException {
		if(btnStop.isEnabled()) {
			frmBramka.setTitle("Bramka");
			btnStop.setEnabled(false);
			btnStart.setEnabled(true);
			btnWe.setEnabled(false);
			btnWy.setEnabled(false);
			return true;
		}
		else return false;
	}
	
	@Override
	public int getNumer() throws RemoteException {
		return ID;
	}
	
	public static void main(String[] args) {
		
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Bramka window = new Bramka();
					window.frmBramka.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Bramka() {
		String address = JOptionPane.showInputDialog("Podaj adres IP rejestru RMI");
		int port = Integer.parseInt(JOptionPane.showInputDialog("Podaj port rejestru RMI"));
		try {
			reg = LocateRegistry.getRegistry(address, port);
			icentrala = (ICentrala) reg.lookup("Centrala");
		    ibramka = (IBramka) UnicastRemoteObject.exportObject(this, 0);
		} catch (RemoteException | NotBoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmBramka = new JFrame();
		frmBramka.setTitle("Bramka");
		frmBramka.setBounds(100, 100, 296, 178);
		frmBramka.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		btnStart = new JButton("Start");
		btnStart.setBounds(31, 31, 78, 23);
		btnStart.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int x = -1;
				try {
					x = icentrala.zarejestrujBramke(ibramka);
				} catch (RemoteException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				if(x>=0) {
					ID = x;
					frmBramka.setTitle("Bramka | ID: " + Integer.toString(ID));
					btnStart.setEnabled(false);
					btnStop.setEnabled(true);
					btnWe.setEnabled(true);
					btnWy.setEnabled(true);
				}
			}
		});
		frmBramka.getContentPane().setLayout(null);
		frmBramka.getContentPane().add(btnStart);
		
		btnStop = new JButton("Stop");
		btnStop.setBounds(176, 31, 78, 23);
		btnStop.setEnabled(false);
		btnStop.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				boolean done = false;
				try {
					done = icentrala.wyrejestrujBramke(ID);
				} catch (RemoteException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				if(done) {
					frmBramka.setTitle("Bramka");
					btnStop.setEnabled(false);
					btnStart.setEnabled(true);
					btnWe.setEnabled(false);
					btnWy.setEnabled(false);
				}
			}
		});
		frmBramka.getContentPane().add(btnStop);
		
		btnWe = new JButton("We");
		btnWe.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				++we;
			}
		});
		btnWe.setBounds(31, 85, 78, 23);
		btnWe.setEnabled(false);
		frmBramka.getContentPane().add(btnWe);
		
		btnWy = new JButton("Wy");
		btnWy.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				++wy;
			}
		});
		btnWy.setBounds(178, 85, 76, 23);
		btnWy.setEnabled(false);
		frmBramka.getContentPane().add(btnWy);
		
		frmBramka.addWindowListener(new WindowAdapter() {
			 
			@Override
			 
			public void windowClosing(WindowEvent e) {
				if(btnStop.isEnabled()) {
					try {
						icentrala.wyrejestrujBramke(ID);
					} catch (RemoteException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}
			}
		});
	}

}


