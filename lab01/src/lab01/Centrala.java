///////////////
//Karol Chmiel
//Nr albumu: 235035
//Termin: pon 11:15 - 13:00
///////////////

package lab01;

import java.awt.EventQueue;
import java.io.Serializable;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import javax.swing.JScrollPane;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.ListSelectionModel;

public class Centrala implements ICentrala, Serializable{
	
	private static final long serialVersionUID = -2351185043156630266L;
	
	private JFrame frmCentrala;
	private IMonitor monitor;
	private JTable table;
	private JButton btnStop;
	private int maxID = 0;

	private ArrayList<Object> zarejestrowaneBramki = new ArrayList<Object>();
	
	@Override
	public int zarejestrujBramke(Object bramka) throws RemoteException {
		boolean status = zarejestrowaneBramki.add((IBramka)bramka);
		if(!status) return -1;
		if(monitor!=null) monitor.koniecznaAktualizacja();
		odswiezTabele();
		btnStop.setEnabled(true);
		return maxID++;
	}

	@Override
	public boolean wyrejestrujBramke(int nrBramki) throws RemoteException {
		for(Object b : zarejestrowaneBramki) {
			IBramka ibramka = (IBramka) b;
			int ID = ibramka.getNumer();
			if(ID == nrBramki) {
				zarejestrowaneBramki.remove(ibramka);
				if(zarejestrowaneBramki.size()==0) btnStop.setEnabled(false);
				if(monitor!=null) monitor.koniecznaAktualizacja();
				odswiezTabele();
				return true;
			}
		}
		return false;
	}

	@Override
	public ArrayList<Object> getZarejestrowaneBramki() throws RemoteException {
		return zarejestrowaneBramki;
	}

	@Override
	public void zarejestrujMonitor(Object o) throws RemoteException {
		monitor = (IMonitor) o;
	}

	@Override
	public void wyrejestrujMonitor() throws RemoteException {
		monitor = null;
	}

	@Override
	public Object getMonitor() throws RemoteException {
		return monitor;
	}

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Centrala window = new Centrala();
					window.initialize();
					window.frmCentrala.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Centrala() {
		int port = Integer.parseInt(JOptionPane.showInputDialog("Podaj port dla rejestru RMI"));
	    try {
			Registry reg = LocateRegistry.createRegistry(port);
			ICentrala icentrala = (ICentrala) UnicastRemoteObject.exportObject(this, 0);
		    reg.rebind("Centrala", icentrala);		
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		monitor = null;
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmCentrala = new JFrame();
		frmCentrala.setTitle("Centrala");
		frmCentrala.setBounds(100, 100, 314, 300);
		frmCentrala.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmCentrala.getContentPane().setLayout(null);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(0, 0, 183, 261);
		frmCentrala.getContentPane().add(scrollPane);
		
		table = new JTable();
		table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		scrollPane.setViewportView(table);
		
		btnStop = new JButton("Stop");
		btnStop.setEnabled(false);
		btnStop.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int index = table.getSelectedRow();
				IBramka ibramka = (IBramka) zarejestrowaneBramki.get(index);
				zarejestrowaneBramki.remove(ibramka);
				if(zarejestrowaneBramki.size()==0) btnStop.setEnabled(false);
				try {
					ibramka.zamknijBramke();
				} catch (RemoteException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				odswiezTabele();
				if(monitor!=null)
					try {
						monitor.koniecznaAktualizacja();
					} catch (RemoteException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
			}
		});
		btnStop.setBounds(193, 108, 95, 34);
		frmCentrala.getContentPane().add(btnStop);
	}
	
	private void odswiezTabele() {
		Thread thread = new Thread(new Runnable() {
			public void run() {
				try {
					Thread.sleep(200);
					
					String col[] = {"ID"};
					DefaultTableModel model = new DefaultTableModel(col, 0);
					
					for(Object b : zarejestrowaneBramki) {
						IBramka ibramka = (IBramka) b;
						try {
							int ID = ibramka.getNumer();
							Object[] stat = {ID};
							model.addRow(stat);
						} catch (RemoteException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
					table.setModel(model);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		thread.start();
	}
}