///////////////
//Karol Chmiel
//Nr albumu: 235035
//Termin: pon 11:15 - 13:00
///////////////

package lab01;

import java.awt.EventQueue;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.Date;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

public class Monitor implements IMonitor {

	private JFrame frmMonitor;
	
	private Registry reg;
	private ICentrala icentrala;
	private IMonitor imonitor;
	private JTable table;
	
	@Override
	public void koniecznaAktualizacja() throws RemoteException {
		Thread thread = new Thread(new Runnable() {
			public void run() {
				try {
					Thread.sleep(200);
					
					ArrayList<Object> bramki = icentrala.getZarejestrowaneBramki();
					String col[] = {"Nr", "We", "Wy"};
					DefaultTableModel model = new DefaultTableModel(col, 0);
					
					
					for(Object b : bramki) {
						IBramka ibramka = (IBramka) b;
						int[] statystyka = ibramka.getStatystyka(new Date(), new Date());
						int ID = ibramka.getNumer();
						Object[] stat = {ID, statystyka[0], statystyka[1]};
						model.addRow(stat);
					}
					table.setModel(model);
				} catch (RemoteException | InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		thread.start();
	}

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Monitor window = new Monitor();
					window.frmMonitor.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Monitor() {
		String address = JOptionPane.showInputDialog("Podaj adres IP rejestru RMI");
		int port = Integer.parseInt(JOptionPane.showInputDialog("Podaj port rejestru RMI"));
		try {
			reg = LocateRegistry.getRegistry(address, port);
			icentrala = (ICentrala) reg.lookup("Centrala");
		    imonitor = (IMonitor) UnicastRemoteObject.exportObject(this, 0);
		    icentrala.zarejestrujMonitor(imonitor);
		} catch (RemoteException | NotBoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    
		initialize();

		Thread thread = new Thread(new Runnable() {
			public void run() {
				try {
					while(true) {
						koniecznaAktualizacja();
						Thread.sleep(5000);
					}
				} catch (RemoteException | InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		thread.start();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmMonitor = new JFrame();
		frmMonitor.setTitle("Monitor");
		frmMonitor.setBounds(100, 100, 317, 300);
		frmMonitor.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmMonitor.getContentPane().setLayout(null);
		
		table = new JTable();
		JScrollPane scrollPane = new JScrollPane(table);
		scrollPane.setBounds(10, 11, 289, 239);
		frmMonitor.getContentPane().add(scrollPane);
		
		frmMonitor.addWindowListener(new WindowAdapter() {
			 
			@Override
			 
			public void windowClosing(WindowEvent e) {
				try {
					icentrala.wyrejestrujMonitor();
				} catch (RemoteException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
	}
}
